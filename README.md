# README #

Accompanying source code for blog entry at http://tech.asimio.net/2017/06/29/Implementing-a-custom-SpringBoot-starter-for-CXF-and-Swagger.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Build starter at https://bitbucket.org/asimio/asimio-api-springboot-starter

### Building and executing the application from command line ###

First build ```asimio-cxf-swagger-springboot-starter``` available at https://bitbucket.org/asimio/asimio-api-springboot-starter to make it available in local .m2 folder or find it in a Maven repo manager after a CI server builds and deploys it.

You could also host Maven artifacts in an AWS S3 bucket as covered at https://tech.asimio.net/2018/06/27/Using-an-AWS-S3-Bucket-as-your-Maven-Repository.html.

```
mvn clean package
java -jar target/asimio-api-starter-demo.jar
or
mvn spring-boot:run
```

### URLs ###

- App available at http://localhost:8080
- API available at

http://localhost:8080/api/v1/hello/{name}

http://localhost:8080/api/hello/{name} with version passed in Accept header

- WADL available at http://localhost:8080/api/?_wadl
- Swagger JSON doc available at http://localhost:8080/api/swagger.json
- Swagger UI available at http://localhost:8080/api/api-docs?url=/api/swagger.json

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero