package com.asimio.api.demo.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.asimio.api.demo.rest" })
public class AsimioApiStarterDemoApp {

	public static void main(String[] args) {
		SpringApplication.run(AsimioApiStarterDemoApp.class, args);
	}
}